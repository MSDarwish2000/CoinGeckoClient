package com.example.coingeckoclient.db.room

import androidx.room.TypeConverter
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class MapConverter {

    @TypeConverter
    fun mapToString(map: Map<String, String?>): String =
        Json.encodeToString(map)

    @TypeConverter
    fun stringToMap(mapJson: String): Map<String, String?> =
        Json.decodeFromString(mapJson)
}
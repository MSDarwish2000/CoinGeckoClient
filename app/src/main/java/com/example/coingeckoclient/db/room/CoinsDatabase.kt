package com.example.coingeckoclient.db.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.coingeckoclient.db.DatabaseSimpleCoin

@Database(entities = [DatabaseSimpleCoin::class], version = 1)
@TypeConverters(MapConverter::class)
abstract class CoinsDatabase: RoomDatabase() {
    abstract val coinsDao: CoinsDao

    companion object {
        const val NAME = "coins"
    }
}
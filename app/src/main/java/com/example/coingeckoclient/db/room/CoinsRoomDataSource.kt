package com.example.coingeckoclient.db.room

import com.example.coingeckoclient.db.CoinsLocalDataSource
import com.example.coingeckoclient.db.DatabaseSimpleCoin

class CoinsRoomDataSource(database: CoinsDatabase) : CoinsLocalDataSource {
    private val coinsDao = database.coinsDao
    override fun observeAllCoins() = coinsDao.observeAllCoins()
    override fun upsertCoins(coins: List<DatabaseSimpleCoin>) = coinsDao.upsertCoins(coins)
}
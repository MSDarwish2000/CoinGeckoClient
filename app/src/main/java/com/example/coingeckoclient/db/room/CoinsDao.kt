package com.example.coingeckoclient.db.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.coingeckoclient.db.DatabaseSimpleCoin
import kotlinx.coroutines.flow.Flow

@Dao
interface CoinsDao {

    @Query("SELECT * FROM Coin")
    fun observeAllCoins(): Flow<List<DatabaseSimpleCoin>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertCoins(coins: List<DatabaseSimpleCoin>)
}
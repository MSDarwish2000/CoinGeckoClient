package com.example.coingeckoclient.db

import kotlinx.coroutines.flow.Flow

interface CoinsLocalDataSource {

    fun observeAllCoins(): Flow<List<DatabaseSimpleCoin>>

    fun upsertCoins(coins: List<DatabaseSimpleCoin>)
}
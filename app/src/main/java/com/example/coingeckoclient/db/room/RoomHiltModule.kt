package com.example.coingeckoclient.db.room

import android.content.Context
import androidx.room.Room
import com.example.coingeckoclient.db.CoinsLocalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RoomHiltModule {

    @Provides
    fun provideCoinsLocalDataSource(@ApplicationContext context: Context): CoinsLocalDataSource =
        CoinsRoomDataSource(
            Room.databaseBuilder(context, CoinsDatabase::class.java, CoinsDatabase.NAME).build()
        )
}
package com.example.coingeckoclient.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.coingeckoclient.models.SimpleCoin

@Entity(tableName = "Coin")
data class DatabaseSimpleCoin(
    @PrimaryKey
    val id: String,
    val name: String,
    val symbol: String,
    val platforms: Map<String, String?> = emptyMap(),
)

fun DatabaseSimpleCoin.toDomainModel() =
    SimpleCoin(id, name, symbol, platforms)
package com.example.coingeckoclient.models

data class SimpleCoin(
    val id: String,
    val name: String,
    val symbol: String,
    val platforms: Map<String, String?> = emptyMap(),
)

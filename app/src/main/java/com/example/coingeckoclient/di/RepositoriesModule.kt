package com.example.coingeckoclient.di

import com.example.coingeckoclient.db.CoinsLocalDataSource
import com.example.coingeckoclient.network.CoinsRemoteDataSource
import com.example.coingeckoclient.repositories.CoinsRepository
import com.example.coingeckoclient.repositories.CoinsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RepositoriesModule {

    @Provides
    fun provideCoinRepository(
        localDataSource: CoinsLocalDataSource,
        remoteDataSource: CoinsRemoteDataSource,
    ): CoinsRepository =
        CoinsRepositoryImpl(localDataSource, remoteDataSource)
}
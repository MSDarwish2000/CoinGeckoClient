package com.example.coingeckoclient.network.retrofit

import com.example.coingeckoclient.network.CoinsRemoteDataSource
import com.example.coingeckoclient.network.HttpException
import com.example.coingeckoclient.network.NetworkSimpleCoin

class CoinsRetrofitDataSource(private val service: CoinGeckoService) : CoinsRemoteDataSource {

    override suspend fun getAllCoins(): List<NetworkSimpleCoin> =
        try {
            service.getAllCoins(true)
        } catch (ex: retrofit2.HttpException) {
            throw HttpException(ex.code(), ex.message())
        }
}
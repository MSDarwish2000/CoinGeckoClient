package com.example.coingeckoclient.network

interface CoinsRemoteDataSource {
    suspend fun getAllCoins(): List<NetworkSimpleCoin>
}
package com.example.coingeckoclient.network.retrofit

import com.example.coingeckoclient.network.CoinsRemoteDataSource
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class RetrofitHiltModule {

    @Provides
    @OptIn(ExperimentalSerializationApi::class)
    fun provideCoinsRemoteDataSource(): CoinsRemoteDataSource {
        val json = Json { coerceInputValues = true }
        val contentType = MediaType.get("application/json")
        val retrofit = Retrofit.Builder()
            .baseUrl(CoinGeckoService.BASE_URL)
            .addConverterFactory(json.asConverterFactory(contentType))
            .build()

        return CoinsRetrofitDataSource(retrofit.create(CoinGeckoService::class.java))
    }
}
package com.example.coingeckoclient.network.retrofit

import com.example.coingeckoclient.network.NetworkSimpleCoin
import retrofit2.http.GET
import retrofit2.http.Query

interface CoinGeckoService {

    @GET("coins/list")
    suspend fun getAllCoins(@Query("include_platform") includePlatform: Boolean): List<NetworkSimpleCoin>

    companion object {
        const val BASE_URL = "https://api.coingecko.com/api/v3/"
    }
}
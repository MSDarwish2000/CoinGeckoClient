package com.example.coingeckoclient.network

class HttpException(
    val errorCode: Int,
    val errorMessage: String,
) : RuntimeException()
package com.example.coingeckoclient.network

import com.example.coingeckoclient.db.DatabaseSimpleCoin
import kotlinx.serialization.Serializable

@Serializable
data class NetworkSimpleCoin(
    val id: String,
    val name: String,
    val symbol: String,
    val platforms: Map<String, String?> = emptyMap(),
)

fun NetworkSimpleCoin.toDatabaseModel() =
    DatabaseSimpleCoin(id, name, symbol, platforms)
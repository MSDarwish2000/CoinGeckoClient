package com.example.coingeckoclient

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoinGeckoClientApplication: Application()
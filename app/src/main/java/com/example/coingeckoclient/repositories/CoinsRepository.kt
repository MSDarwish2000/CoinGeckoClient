package com.example.coingeckoclient.repositories

import com.example.coingeckoclient.db.CoinsLocalDataSource
import com.example.coingeckoclient.db.toDomainModel
import com.example.coingeckoclient.models.SimpleCoin
import com.example.coingeckoclient.network.CoinsRemoteDataSource
import com.example.coingeckoclient.network.toDatabaseModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface CoinsRepository {
    val coins: Flow<List<SimpleCoin>>

    suspend fun refreshCoins()
}

class CoinsRepositoryImpl(
    private val localDataSource: CoinsLocalDataSource,
    private val remoteDataSource: CoinsRemoteDataSource,
) : CoinsRepository {
    override val coins: Flow<List<SimpleCoin>> =
        localDataSource.observeAllCoins().map { list ->
            list.map { it.toDomainModel() }
        }

    override suspend fun refreshCoins() {
        val newCoins = remoteDataSource.getAllCoins().map { it.toDatabaseModel() }
        localDataSource.upsertCoins(newCoins)
    }
}
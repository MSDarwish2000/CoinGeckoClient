package com.example.coingeckoclient.ui.coinslist

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.coingeckoclient.R
import com.example.coingeckoclient.network.HttpException
import com.example.coingeckoclient.repositories.CoinsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class CoinsListViewModel @Inject constructor(
    private val coinsRepository: CoinsRepository
) : ViewModel() {

    /*
     * States
     */

    val coins = coinsRepository.coins

    private val _isRefreshing = MutableStateFlow(false)
    val isRefreshing = _isRefreshing.asStateFlow()

    init {
        refreshCoins()
    }

    /*
     * Events
     */

    private val _showMessage = MutableSharedFlow<Int>()
    val showMessage = _showMessage.asSharedFlow()

    private fun showMessage(@StringRes messageId: Int) {
        viewModelScope.launch {
            _showMessage.emit(messageId)
        }
    }

    /*
     * Intents
     */

    fun refreshCoins() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _isRefreshing.value = true
                coinsRepository.refreshCoins()
            } catch (ex: IOException) {
                showMessage(R.string.error_connecting)
            } catch (ex: HttpException) {
                showMessage(R.string.error_http)
            } finally {
                _isRefreshing.value = false
            }
        }
    }
}
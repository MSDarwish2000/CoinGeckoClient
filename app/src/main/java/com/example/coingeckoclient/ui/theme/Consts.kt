package com.example.coingeckoclient.ui.theme

import androidx.compose.ui.unit.dp

val SmallPadding = 4.dp
val MediumPadding = 8.dp
val LargePadding = 12.dp
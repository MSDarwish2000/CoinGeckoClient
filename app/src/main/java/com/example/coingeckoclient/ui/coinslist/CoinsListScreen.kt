package com.example.coingeckoclient.ui.coinslist

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.coingeckoclient.R
import com.example.coingeckoclient.models.SimpleCoin
import com.example.coingeckoclient.ui.theme.CoinGeckoClientTheme
import com.example.coingeckoclient.ui.theme.LargePadding
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

@Composable
fun CoinsListScreen(
    viewModel: CoinsListViewModel = hiltViewModel(),
) {
    val snackbarHostState = remember { SnackbarHostState() }
    val resources = LocalContext.current.resources

    /*
     * States
     */

    val coins by viewModel.coins.collectAsState(initial = null)
    val isRefreshing by viewModel.isRefreshing.collectAsState()

    /*
     * Events
     */

    LaunchedEffect(Unit) {
        viewModel.showMessage.collect { messageId ->
            snackbarHostState.showSnackbar(message = resources.getString(messageId))
        }
    }

    /*
     * Layout
     */

    CoinsListLayout(
        coins = coins ?: emptyList(),
        scaffoldState = rememberScaffoldState(snackbarHostState = snackbarHostState),
        isRefreshing = isRefreshing || coins == null,
        onRefresh = { viewModel.refreshCoins() },
    )
}

@Composable
fun CoinsListLayout(
    coins: List<SimpleCoin>,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    isRefreshing: Boolean = false,
    onRefresh: () -> Unit = {},
) {
    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            Column {
                TopAppBar(
                    title = {
                        Text(text = stringResource(R.string.app_title))
                    },
                )
            }
        },
    ) { paddingValues ->
        SwipeRefresh(
            state = rememberSwipeRefreshState(isRefreshing),
            onRefresh = onRefresh,
            modifier = Modifier
                .padding(paddingValues)
                .fillMaxSize(),
        ) {
            CoinsList(
                coins = coins,
                modifier = Modifier.fillMaxSize(),
                contentPadding = PaddingValues(LargePadding),
            )
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun CoinsListLayoutPreview() {
    val sampleCoins = listOf(
        SimpleCoin("bitcoin", "Bitcoin", "btc"),
        SimpleCoin("ethereum", "Ethereum", "eth"),
    )

    CoinGeckoClientTheme {
        CoinsListLayout(sampleCoins)
    }
}


@Preview(showBackground = true, showSystemUi = true)
@Composable
fun EmptyCoinsListLayoutPreview() {
    CoinGeckoClientTheme {
        CoinsListLayout(emptyList())
    }
}
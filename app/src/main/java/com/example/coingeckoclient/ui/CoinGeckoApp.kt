package com.example.coingeckoclient.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.coingeckoclient.ui.coinslist.CoinsListScreen
import com.example.coingeckoclient.ui.theme.CoinGeckoClientTheme

@Composable
fun CoinGeckoClientApp(isDarkTheme: Boolean = isSystemInDarkTheme()) {
    CoinGeckoClientTheme(isDarkTheme) {
        NavHost(navController = rememberNavController(), startDestination = HOME_DESTINATION) {
            composable(HOME_DESTINATION) {
                CoinsListScreen()
            }
        }
    }
}

private const val HOME_DESTINATION = "home"
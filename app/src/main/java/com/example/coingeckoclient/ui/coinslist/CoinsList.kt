package com.example.coingeckoclient.ui.coinslist

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.coingeckoclient.R
import com.example.coingeckoclient.models.SimpleCoin
import com.example.coingeckoclient.ui.theme.CoinGeckoClientTheme
import com.example.coingeckoclient.ui.theme.LargePadding
import com.example.coingeckoclient.ui.theme.MediumPadding

@Composable
fun CoinsList(
    coins: List<SimpleCoin>,
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp),
) {
    if (coins.isEmpty()) {
        Box(modifier.padding(contentPadding)) {
            Text(
                text = stringResource(R.string.no_coins),
                modifier = Modifier.align(Alignment.Center),
            )
        }
    } else {
        LazyColumn(modifier, contentPadding = contentPadding) {
            items(coins) {
                CoinCard(
                    coin = it,
                    modifier = Modifier
                        .padding(vertical = MediumPadding)
                        .fillMaxWidth(),
                )
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CoinCard(
    coin: SimpleCoin,
    modifier: Modifier = Modifier,
) {
    Card(modifier, elevation = 6.dp) {
        Column(Modifier.padding(horizontal = LargePadding, vertical = MediumPadding)) {
            Text(
                text = coin.symbol.uppercase(),
                color = LocalContentColor.current.copy(alpha = ContentAlpha.medium),
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
                style = MaterialTheme.typography.overline,
            )

            Text(
                text = coin.name,
                fontSize = 28.sp,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
                style = MaterialTheme.typography.subtitle2,
            )

            Spacer(Modifier.height(MediumPadding))

            val platformsCount = coin.platforms.size
            Text(
                text = pluralStringResource(
                    id = R.plurals.platforms,
                    count = platformsCount,
                    platformsCount,
                ),
                style = MaterialTheme.typography.caption,
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CoinCardPreview() {
    val sampleCoin = SimpleCoin("bitcoin", "Bitcoin", "btc")

    CoinGeckoClientTheme {
        CoinCard(sampleCoin, Modifier.padding(LargePadding))
    }
}
# CoinGecko Client

![Light Preview](/docs/light_preview.jpg)
![Dark Preview](/docs/dark_preview.jpg)

This is a test task by Mohamed S Darwish for Longevity InTime.

## Stack (as mentioned in the task entry)

- Jetpack Compose
- MVVM
- Kotlin Coroutines
- Dagger's Hilt
- Room
- Retrofit
- Kotlinx Serialization
- AndroidX Compose Navigation

## What makes the project special?

The project was made with Clean Architecture concepts in mind. This, for example, allows for extending its functionality to include more API calls, more screens, and more features with minimal to no changes to the current code base. This also allows it to be converted to a multi-module project easily, and makes the code much more testable and maintainable.

The project also implements good UI with ability to refresh, and simple exception-handling that just works.

## Concerns

- **Why didn't you implement tests?**
  
  First, for shortage of time, this was left for further improvements. Second, the code doesn't have much of complex logic. Of course, having tests is better than not.

- **Why didn't you modularize the project?**

  The project is simple enough to be in one module with the potentiality of easy conversion to multi-module project.

- **Why did you use different models for domain, network, and database?**

  It is a well-known pattern of course. It may appear to be unneeded for this task, but it is of great benefit with the possibility of future API updates with more unwanted fields, the possiblity of needing to replace Room with Sqldelight or Retrofit with Ktor for example to re-use the code on other platforms, better separation of concerns, and avoiding Room-generated and serialization-generated code in the domain layer.

- **Why did you add navigation to a single-screen application?**

  As already mentioned, it is mainly to be future-proof and to properly scope the view model.

- **Why did you commit directly to main?**

  As it is just solo and small application, it was time saving specially during the initial stages. For real workflows, I usually use branching/pull request workflows as evident in [my GitHub repos](https://github.com/MayakaApps/).

- **Why did you take around 6 hours?**

  I was just doing my daily activities while programming as it is not a job yet. Also, building a good infrastructure and architecture is more time-consuming than implementing new features. This is evident as creating the networking and database classes themselves took less than one fifth of the total time. Also, I had to imagine how the cards looked like and tweak them myself, which is not applicable when using an already-made design.

**Thanks ❤️**
